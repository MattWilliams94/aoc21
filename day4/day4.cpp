#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <cmath>

const std::string WHITESPACE = " \n\r\t\f\v";
 
std::string ltrim(const std::string &s)
{
    size_t start = s.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : s.substr(start);
}
 
std::string rtrim(const std::string &s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}
 
std::string trim(const std::string &s) 
{
    return rtrim(ltrim(s));
}

std::string vec2string (std::vector<int> vector)
{
    std::string output;
    for ( auto element = vector.begin() ; element != vector.end() ; element++)
    {
        output.append(std::to_string(*element));
        output.append(" ");
    }
    return output;
}

std::vector<std::vector<std::vector<int>>> checkForDrawNumber (int searchVal, std::vector<std::vector<std::vector<int>>> boards, std::vector<std::vector<std::vector<int>>> &tracker)
{
    for (int board = 0; board < boards.size(); board++ )
    {
        for (int line = 0; line < boards[board].size(); line++ )
        {
            for (int element = 0; element < boards[board][line].size(); element++ )
            {
                if(boards[board][line][element] == searchVal)  {tracker[board][line][element] = 1;}
            }
        }
    }
    return tracker;
}

bool checkForBingo (std::vector<std::vector<int>> &tracker)
{
    for (int line = 0; line < tracker.size(); line++ )
    {
        int rowCount{0};
        for (int element = 0; element < tracker[line].size(); element++ )
        { 
            if(tracker[line][element] == 1)  {rowCount++;}
            if(rowCount==5)                  {return true;}
        }
    }

    for (int line = 0; line < tracker.size(); line++ )
    {
        int colCount{0};
        for (int element = 0; element < tracker[line].size(); element++ )
        { 
            if(tracker[element][line] == 1)  {colCount++;}
            if(colCount==5)                  {return true;}
        }
    }

    return false;
}

int sumUncalled (std::vector<std::vector<int>> &board, std::vector<std::vector<int>> &tracker)
{
    int sumOfUncalled{0};
    for (int line = 0; line < tracker.size(); line++ )
    {
        for (int element = 0; element < tracker[line].size(); element++ )
        { 
            if(tracker[line][element] == 0)  {sumOfUncalled += board[line][element];}
        }
    }
    return sumOfUncalled;
}

void printBoard (std::vector<std::vector<int>> &board)
{
    for (int line = 0; line < board.size(); line++ )
    {
        for (int element = 0; element < board[line].size(); element++ )
        {
            std::cout << board[line][element] << " ";
        }
        std::cout << "\n";
    }
}

int main()
{
    std::ifstream   file("input.txt");
    std::string     contents((  std::istreambuf_iterator<char>(file)), 
                                std::istreambuf_iterator<char>());

    int                     lineDelimPos        {0};
    int                     doubleLineDelimPos  {0};
    int                     spaceDelimPos       {0};
    int                     commaDelimPos       {0};
    std::string             lineDelim           {"\n"};
    std::string             doubleLineDelim     {"\n\n"};
    std::string             commaDelim          {","};
    std::string             spaceDelim          {" "};
    std::string             line;

    // get first line (number to be drawn) and populate 'drawNumbers'
    std::vector<int> drawNumbers;
    lineDelimPos = contents.find(lineDelim);
    line = contents.substr(0, lineDelimPos);
    contents.erase(0, lineDelimPos + lineDelim.length()+1);
    while ((commaDelimPos = line.find(commaDelim)) != std::string::npos) 
    {
        std::string value = line.substr(0, commaDelimPos);
        drawNumbers.push_back(std::stoi(value));
        line.erase(0, commaDelimPos + commaDelim.length());
    }

    // get remainder of input (all bingo boards) and populate 'allBoards'
    std::string boardString;
    std::string boardLineString;
    std::string boardElement;
    int         value;
    std::vector<std::vector<std::vector<int>>>   allBoards;
    while (!contents.empty()) 
    {
        doubleLineDelimPos  = contents.find(doubleLineDelim);
        boardString         = contents.substr(0, doubleLineDelimPos);
        contents.erase(0, doubleLineDelimPos + doubleLineDelim.length());
        std::vector<std::vector<int>> board;

        while (!boardString.empty())
        {
            lineDelimPos    = boardString.find(lineDelim);
            boardLineString = boardString.substr(0, lineDelimPos);
            boardString.erase(0, boardLineString.size() + lineDelim.length());
            std::vector<int> boardLine;
            
            while (!boardLineString.empty())
            {
                spaceDelimPos = boardLineString.find(spaceDelim);
                boardElement  = boardLineString.substr(0, spaceDelimPos);
                boardLineString.erase(0, boardElement.size() + spaceDelim.length());
                try{ boardLine.push_back(std::stoi(trim(boardElement))); } catch(const std::exception& e){}
            }
            board.push_back(boardLine);
            
        } 
        allBoards.push_back(board);
    }

    // make a copy of allBoards and set all elements to zero, so we can map which elements have been called
    std::vector<std::vector<std::vector<int>>>   allBoardsTracker = allBoards;
    for (int board = 0; board < allBoardsTracker.size(); board++ )
    {
        for (int line = 0; line < allBoardsTracker[board].size(); line++ )
        {
            for (int element = 0; element < allBoardsTracker[board][line].size(); element++ )
            {
                allBoardsTracker[board][line][element] = 0;
            }
        }
        //printBoard   (allBoardsTracker[board]);
        //std::cout << "\n";
    }
/*
    // Part 1
    bool bingoFound{false};
    int  numDrawnWhenWon{0};
    for (int drawnNumber : drawNumbers)
    {
        if(bingoFound)  {break;}
        allBoardsTracker = checkForDrawNumber(drawnNumber, allBoards, allBoardsTracker);
        for (int board = 0; board < allBoardsTracker.size(); board++ )
        {
            if(checkForBingo(allBoardsTracker[board])) 
            {
                printBoard(allBoardsTracker[board]); 
                printBoard(allBoards[board]);
                std::cout << sumUncalled(allBoards[board],allBoardsTracker[board]) << "\n"; 
                numDrawnWhenWon = drawnNumber;
                std::cout << numDrawnWhenWon << "\n";
                std::cout << numDrawnWhenWon*sumUncalled(allBoards[board],allBoardsTracker[board]) << "\n";
                bingoFound = true;
            }
        }
    } 
*/

    for (int board = 0; board < allBoards.size(); board++ )
    {
        //printBoard(allBoards[board]);
        //std::cout << "\n";
    }
/*
    // Part 2
    bool bingoFoundP2{false};
    for (int drawnNumber : drawNumbers)
    {
        if(allBoards.size() <= 1)   {break;}
        allBoardsTracker = checkForDrawNumber(drawnNumber, allBoards, allBoardsTracker);
        for (int board = 0; board < allBoardsTracker.size(); board++ )
        {
            if(checkForBingo(allBoardsTracker[board])) 
            {
                //printBoard(allBoardsTracker[board]); 
                //printBoard(allBoards[board]);
                //std::cout << "here?\n";
                allBoardsTracker[board].erase(allBoardsTracker[board].begin(),allBoardsTracker[board].end());
                //std::cout << "how about here?\n";
                allBoards[board].erase(allBoards[board].begin(),allBoards[board].end());
                std::cout << allBoards.size() << "\n";
                //std::cout << "or here?\n";
                board--;
            }
        }
    }
    // printBoard(allBoards[0]);
    //std::cout << allBoards.size() << "\n";
    //std::cout << allBoardsTracker.size() << "\n";
*/
    std::cout << allBoards.size() << "\n";
    allBoards[2].erase(allBoards[2].begin()+1);
    allBoards[3].erase(allBoards[3].begin()+1);
    std::cout << allBoards.size() << "\n";

    for (int board = 0; board < allBoards.size(); board++ )
    {
        printBoard(allBoards[board]);
        std::cout << "\n";
        // allBoards[board].erase(allBoards[board].begin(),allBoards[board].begin()+1);
    }

    return 0;
}
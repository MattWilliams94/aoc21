#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <cmath>

std::vector<std::vector<int>> inputAsVector   (std::string contents)
{
    int                     lineDelimPos    {0};
    int                     spaceDelimPos   {0};
    std::string             lineDelim       {"\n"};
    std::string             spaceDelim      {" "};
    std::string             line;

    std::vector<std::vector<int>>   output;

    while ((lineDelimPos = contents.find(lineDelim)) != std::string::npos) 
    {
        line = contents.substr(0, lineDelimPos);
        std::vector<int> lineVec;
        for (int i = 0 ; i < line.size() ; i++)
        {
            lineVec.push_back(std::stoi(line.substr(i, 1)));
        }
        output.push_back(lineVec);
        contents.erase(0, lineDelimPos + lineDelim.length());
    }

    return output;
}

std::string vec2string (std::vector<int> vector)
{
    std::string output;
    for ( auto element = vector.begin() ; element != vector.end() ; element++)
    {
        output.append(std::to_string(*element));
    }
    return output;
}

int main()
{
    std::ifstream   file("input.txt");
    std::string     contents((  std::istreambuf_iterator<char>(file)), 
                                std::istreambuf_iterator<char>());

    std::vector<std::vector<int>>  input  = inputAsVector(contents);

    // Part 1
    std::vector<int> gammaBitCount0(input[0].size(),0);
    std::vector<int> gammaBitCount1(input[0].size(),0);
    for (std::vector<int> line : input)
    {
        // Gamma bit
        for (int i = 0 ; i < line.size() ; i++)
        {
            line[i] == 0 ? gammaBitCount0[i]++ : gammaBitCount1[i]++;
        }
    }

    std::bitset<12> gammaRate;
    for (int i = 0 ; i < gammaBitCount0.size() ; i++)
    {
        std::bitset<12> mask{(long long unsigned int)pow(2,gammaBitCount0.size()-i-1)};
        if (gammaBitCount0[i] < gammaBitCount1[i]) {gammaRate|=mask;}
    }

    std::cout << "epsilRate\t\t"            << gammaRate.flip() << "\n";
    std::cout << "gammaRateInDec\t\t"       << gammaRate.to_ulong() << "\n";
    std::cout << "epsilRateInDec\t\t"       << gammaRate.flip().to_ulong() << "\n";
    std::cout << "gammaRate*epsilRate\t"    << gammaRate.to_ulong()*gammaRate.flip().to_ulong() << "\n\n";


    // Part 2
    bool o2more1s{false};
    bool co2more1s{false};
    int bitCount0{0};
    int bitCount1{0};
    std::vector<std::vector<int>>  inputForO2  = input;
    std::vector<std::vector<int>>  inputForCO2  = input;
    std::vector<int> o2GenRating;
    std::vector<int> co2ScrubRating;
    for (int column = 0 ; column < input[0].size() ; column++)
    {
        // consider most common element in latest O2 vector (0 or 1)
        bitCount0 = 0;
        bitCount1 = 0;
        for (std::vector<int> line : inputForO2)
        {
            if (inputForO2.size() > 1)
            {
                line[column] == 0 ? bitCount0++ : bitCount1++;
            }
        }
        bitCount0 <= bitCount1 ? o2more1s=true : o2more1s=false;

        // consider oxygen generator rating
        std::vector<std::vector<int>>::iterator rowO2;
        for (rowO2 = inputForO2.begin() ; rowO2 != inputForO2.end() ; rowO2++)
        {
            std::vector<int> line = *rowO2;
            if      ( (line[column] == 0) & ( o2more1s)) {inputForO2.erase(rowO2); rowO2--; }
            else if ( (line[column] == 1) & ( !o2more1s)) {inputForO2.erase(rowO2); rowO2--; }

            // if its the last element, break out
            if      (inputForO2.size() <= 1) {o2GenRating = *rowO2; break;}
        }

        // consider most common element in latest CO2 vector (0 or 1)
        bitCount0 = 0;
        bitCount1 = 0;
        for (std::vector<int> line : inputForCO2)
        {
            if (inputForCO2.size() > 1)
            {
                line[column] == 0 ? bitCount0++ : bitCount1++;
            }
        }
        bitCount0 <= bitCount1 ? co2more1s=true : co2more1s=false;

        // consider CO2 scrubber rating
        std::vector<std::vector<int>>::iterator rowCO2;
        for (rowCO2 = inputForCO2.begin() ; rowCO2 != inputForCO2.end() ; rowCO2++)
        {
            if (inputForCO2.size() <= 1) {co2ScrubRating = *rowCO2;}

            std::vector<int> line = *rowCO2;
            if      ( (line[column] == 0) & ( !co2more1s)) {inputForCO2.erase(rowCO2); rowCO2--; }
            else if ( (line[column] == 1) & ( co2more1s)) {inputForCO2.erase(rowCO2); rowCO2--; }
        }
    }

    std::bitset<12> o2GenRatingBin;
    std::bitset<12> co2ScrubRatingBin;
    for (int i = 0 ; i < 12 ; i++)
    {
        if(o2GenRating[i]    == 1)  {o2GenRatingBin.set(11-i,true);}
        if(co2ScrubRating[i] == 1)  {co2ScrubRatingBin.set(11-i,true);}
    }

    std::cout << "o2GenRating\t\t\t" << o2GenRatingBin << "\n";
    std::cout << "co2ScrubRating\t\t\t" << co2ScrubRatingBin << "\n";
    std::cout << "o2GenRating*co2ScrubRating\t" << o2GenRatingBin.to_ulong()*co2ScrubRatingBin.to_ulong() << "\n\n";

    return 0;
}
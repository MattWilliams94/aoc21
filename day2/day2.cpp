#include <iostream>
#include <fstream>
#include <vector>

struct commands
{
    std::string direction;
    int         distance;
};

std::vector<commands> inputAsVector   (std::string contents)
{
    int                     lineDelimPos    {0};
    int                     spaceDelimPos   {0};
    std::string             lineDelim       {"\n"};
    std::string             spaceDelim      {" "};
    std::string             line;
    std::vector<commands>   output;

    while ((lineDelimPos = contents.find(lineDelim)) != std::string::npos) 
    {
        line                = contents.substr(0, lineDelimPos);
        spaceDelimPos       = line.find(spaceDelim);

        commands  command;
        command.direction   = line.substr(0, spaceDelimPos);
        line.erase(0, spaceDelimPos + 1);
        command.distance    = std::stoi(line.substr(0, spaceDelimPos));

        output.push_back(command);
        contents.erase(0, lineDelimPos + lineDelim.length());
    }

    return output;
}

int main()
{
    std::ifstream   file("input.txt");
    std::string     contents((  std::istreambuf_iterator<char>(file)), 
                                std::istreambuf_iterator<char>());

    std::vector<commands>  input  = inputAsVector(contents);

    int horiP1{0};
    int dpthP1{0};
    int horiP2{0};
    int dpthP2{0};
    int aim{0};
    for (commands command : input)
    {
        if      (command.direction == "forward" ) 
        {
            // Part 1
            horiP1 += command.distance;

            // Part 2
            horiP2 += command.distance;
            dpthP2 += aim*command.distance;
        }
        else if (command.direction == "up"      ) 
        {
            // Part 1
            dpthP1 -= command.distance;            

            // Part 2
            aim -= command.distance;
        }
        else if (command.direction == "down"    ) 
        {
            // Part 1
            dpthP1 += command.distance;

            // Part 2
            aim += command.distance;
        }
    }

    std::cout << "Part 1: " << horiP1*dpthP1 << "\n";
    std::cout << "Part 2: " << horiP2*dpthP2 << "\n";

    return 0;
}

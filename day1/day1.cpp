#include <iostream>
#include <fstream>
#include <vector>

std::vector<int> inputAsVector   (std::string contents)
{
    int                 lineDelimPos    {0};
    std::string         lineDelim       {"\n"};
    std::string         line;
    std::vector<int>    input;

    while ((lineDelimPos = contents.find(lineDelim)) != std::string::npos) 
    {
        input.push_back(std::stoi(contents.substr(0, lineDelimPos)));
        contents.erase(0, lineDelimPos + lineDelim.length());
    }

    return input;
}

int main()
{
    std::ifstream   file("input.txt");
    std::string     contents((  std::istreambuf_iterator<char>(file)), 
                                std::istreambuf_iterator<char>());

    std::vector<int>  input  = inputAsVector(contents);

    // Part 1
    int depthIncCount{0};
    for (int i = 1 ; i < input.size() ; i++)
    {
        if(input[i] > input[i-1])
        {
            depthIncCount++;
        }
    }

    // Part 2
    int windowIncCount{0};
    for (int i = 3 ; i < input.size() ; i++)
    {
        if((input[i]+input[i-1]+input[i-2]) > (input[i-1]+input[i-2]+input[i-3]))
        {
            windowIncCount++;
        }
    }

    std::cout << "Part 1: " << depthIncCount << "\n";
    std::cout << "Part 2: " << windowIncCount << "\n";
    return 0;
}
